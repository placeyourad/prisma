// const {GraphQLServer} = require('graphql-yoga');
// const {Prisma} = require('prisma-binding');
// const resolvers = require('./resolvers');
// require('dotenv').config()
//
// const server = new GraphQLServer({
//     typeDefs: 'src/schema.graphql',
//     resolvers,
//     context: req => ({
//         req,
//         prisma: new Prisma({
//             typeDefs: 'src/generated/prisma.graphql',
//             endpoint:
//             process.env.URL_DB_PRISMA,
//         }),
//     }),
// })
//
// server.start(
//     () => console.log(`GraphQL server is running on http://localhost:4000`)
// )

const { ApolloServer, gql } = require('apollo-server');
// A schema is a collection of type definitions (hence "typeDefs")
// that together define the "shape" of queries that are executed against
// your data.
const typeDefs = gql`

<<<<<<< HEAD
=======
    type User {
        
    }
    
    type Query {
        users: [User]
    }
    
    type Mutation {
        createUser(email: String, password: String, role: json): User
    }
    
    schema {
        query: Query
        mutation: Mutation
    }

>>>>>>> refactoPrisma
    type Book {
        title: String
        author: String
        description: String
    }
    type Query {
        books: [Book]
    }
    type Mutation {
        createBook(title: String, author: String, description: String): Book
    }
    schema {
        query: Query
        mutation: Mutation
    }
`;
const books = [
    {
        title: 'Harry Potter and the Chamber of Secrets',
        author: 'J.K. Rowling',
        description: 'testing shit'
    },
    {
        title: 'Jurassic Park',
        author: 'Michael Crichton',
        description: 'Shit is getting real'
    },
    {
        title: 'test book',
        author: 'Moses west',
        description: 'This is a demo graphQL project'
    }
];

//
function save ({title, author, description}) {
    let item = {title, author, description}
    books.unshift(item)
    return item
}

// Resolvers define the technique for fetching the types defined in the
// schema. This resolver retrieves books from the "books" array above.
const resolvers = {
    Query: {
        books: () => books,
    },
    Mutation: {
        async createBook (_, {title, author, description}) {
            return await save({title, author, description})
        }
    }
};



const server = new ApolloServer({ typeDefs, resolvers });
// The `listen` method launches a web server.
server.listen().then(({ url }) => {
    console.log(`?  Server ready at ${url}`);
});