import Client from '@prisma/client'
const prisma = new Client.PrismaClient()
async function main() {
    //creation user (fonctionnelle)
    // await prisma.user.create({
    //     data: {
    //         id: 1,
    //         email: 'blablabla@gmail.com',
    //         roles: [ 'ROLE_ADVERTISER' ],
    //         password: "password"
    //     },
    // })
    const allUsers = await prisma.user.findMany()
    console.log(allUsers)
}
main()
    .catch(e => {
        throw e
    })
    .finally(async () => {
        await prisma.$disconnect()
    })