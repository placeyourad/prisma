# placeYourAd-prisma

## Run app

Run your docker then :

`npm start`

## Prisma introspect

Generate model from postgres

`npx prisma introspect`

## Deploy

On every change of a service file

`prisma1 deploy`
`npx prisma introspect`

`npx ts-node index.js`
